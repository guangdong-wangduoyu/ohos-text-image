package com.pvj.textimage.slice;

import com.pvj.textimage.ResourceTable;
import com.pvj.textimagetitlelibrary.L;
import com.pvj.textimagetitlelibrary.TextImageTitle;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        text0();
        text1();
        text2();
        text3();
//        text4();
//        text5();
//        text6_0();
    }

    private void text0() {
        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text0);
        TextImageTitle.Parameter parameter = new TextImageTitle.Parameter().
                setMaxTextLines(1).
                setImageLayout(TextImageTitle.LAYOUT_FRONT).
                setImageResId(ResourceTable.Media_icon_notice).
                setLineHeight(80).
                setTextSize(50).
                setImageWidth(100).setImageHeight(80).
                setImageMarginRight(30).
                setImageMarginLeft(0);
        textImageTitle.setParameter(parameter);
        textImageTitle.setText("鸿蒙3.0有望在10月22日发布");
    }

    private void text1() {
        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text1);
        Component component = LayoutScatter.getInstance(getContext()).
                parse(ResourceTable.Layout_icon_tv2, textImageTitle, false);
        TextImageTitle.Parameter parameter = new TextImageTitle.Parameter().
                setMaxTextLines(1).
                setImageLayout(TextImageTitle.LAYOUT_TAIL).
                setImageComponent(component).
                setLineHeight(70).
                setTextSize(45).
                setImageWidth(400).setImageHeight(70).
                setImageMarginLeft(30);
        textImageTitle.setParameter(parameter);
        textImageTitle.setText("HarmonyOS 图文混排标题!");
    }

    private void text2() {
        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text2);
        TextImageTitle.Parameter parameter = new TextImageTitle.Parameter().
                setMaxTextLines(-1).
                setImageLayout(TextImageTitle.LAYOUT_TAIL).
                setLineHeight(70).
                setTextSize(45).
                setImageResId(ResourceTable.Media_icon_hot).
                setImageWidth(70).setImageHeight(70).
                setImageMarginLeft(10);
        textImageTitle.setParameter(parameter);
        textImageTitle.setText("百亿产业是数字，十万从业者是赤子｜电子竞技在中国05观后感");
    }

    private void text3() {
        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text3);
        TextImageTitle.Parameter parameter = new TextImageTitle.Parameter().
                setMaxTextLines(3).
                setImageLayout(TextImageTitle.LAYOUT_TAIL).
                setImageResId(ResourceTable.Media_icon_more).
                setImageWidth(70).setImageHeight(70).
                setLineHeight(70).
                setTextSize(45).
                setImageMarginLeft(0).
                setImageMarginRight(50);
        textImageTitle.setParameter(parameter);
        textImageTitle.setText("华为开发者大会2021将在东莞松山湖举办，根据官方邀请函上透露出来的信息，鸿蒙HarmonyOS 3.0、HMS Core 6、全屋智能等黑科技也将悉数亮相。\n" +
                "\n" +
                "鸿蒙HarmonyOS 2升级用户数量已经突破了1.3亿大关，并且华为鸿蒙OS系统目前平均每天升级用户数量都超过了100百万，根据华为目前的规划，鸿蒙OS系统的百机升级计划将会在今年12月份前后完成。");
        textImageTitle.setImageClickedListener(() -> {
            L.d("TextImageTitle", "image click....");
            // TODO do something....
        });
    }

    private void text4() {
//        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text4);
//        textImageTitle.setMaxTextLines(-1);
//        textImageTitle.setImageLayout(TextImageTitle.LAYOUT_FRONT);
//        textImageTitle.setPixelMap(ResourceTable.Media_icon_more);
//        textImageTitle.setImageSize(240, 103);
//        textImageTitle.setImageMarginLeft(30);
//        textImageTitle.setText("在鸿蒙中Text多行文字尾部显示图片阿斯顿");
    }

    private void text5() {
//        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text5);
//        textImageTitle.setMaxTextLines(3);
//        textImageTitle.setImageLayout(TextImageTitle.LAYOUT_FRONT);
//
//        Component component = LayoutScatter.getInstance(getContext()).
//                parse(ResourceTable.Layout_icon_tv, textImageTitle, false);
//        textImageTitle.setImageComponent(component);
//        textImageTitle.setImageSize(240, 103);
//        textImageTitle.setImageMarginLeft(30);
//        textImageTitle.setImageMarginRight(160);
//        textImageTitle.setText("在鸿蒙中Text多行文字尾部显示图片阿斯顿多行文字尾部多行文字尾部多行文ada字尾部aa!");
    }


    private void text6_0() {
//        TextImageTitle textImageTitle = (TextImageTitle) findComponentById(ResourceTable.Id_text6_0);
//        textImageTitle.setMaxTextLines(1);
//        textImageTitle.setImageLayout(TextImageTitle.LAYOUT_FRONT);
//        Component component = LayoutScatter.getInstance(getContext()).
//                parse(ResourceTable.Layout_icon_tv, textImageTitle, false);
//        component.setWidth(240);
//        component.setHeight(103);
//        textImageTitle.setImageComponent(component);
//        textImageTitle.setImageSize(240, 103);
//        textImageTitle.setImageMarginLeft(30);
//        textImageTitle.setText("单行文字短中尾部+图片");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
